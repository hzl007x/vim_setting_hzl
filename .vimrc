"corlorful codes
syntax enable

colorscheme badwolf

"set shortcut for easy comment and uncomment (C) 
nmap rr _i/*<esc>A*/<esc>
nmap tt _dldl$hdldl<esc>

"set tab -> space
set tabstop=2
set shiftwidth=2
set expandtab 

"set number
set number

"light the line and column
set cursorcolumn
set cursorline

"NERTD-Tree set up
map <C-n> :NERDTreeToggle<CR>

"vimtex-pdfviewer setup
let g:vimtex_quickfix_mode = 0

let g:vimtex_view_general_viewer = 'okular'
let g:vimtex_view_general_options = '--unique file:@pdf\#src:@line@tex'
let g:vimtex_view_general_options_latexmk = '--unique'

"vimtex complier
let g:Tex_MultipleCompileFormats='pdf,bib,pdf,pdf'

"snippet
let g:UltiSnipsExpandTrigger = '<c-j>'
let g:UltiSnipsJumpForwardTrigger = '<c-j>'
let g:UltiSnipsJumpBackwardTrigger = '<c-k>'

"Termdebug set up
let g:termdebug_wide = 1

"languagetool set up
:let g:languagetool_jar='$HOME/LanguageTool-5.2/languagetool-commandline.jar'
:let g:languagetool_disable_rules='COMMA_PARENTHESIS_WHITESPACE,WORD_CONTAINS_UNDERSCORE,CURRENCY,ARROWS,WHITESPACE_RULE,EN_UNPAIRED_BRACKETS,EN_QUOTES'

" Vundle配置
set nocompatible              " 设置vim和vi不兼容。在兼容模式下运行时，Vim 大部分增强及改善的功能就不可用了,vundle要求必须有
filetype off                  " required
" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

"vimtex
Plugin 'lervag/vimtex'
let g:tex_flavor='latex'
"NERD-Tree
Plugin 'preservim/nerdtree'
"white color-scheme for vim
Plugin 'vim-scripts/Cleanroom'
"Snippet
Plugin 'SirVer/ultisnips'
"YouCompleteMe 
Plugin 'Valloric/YouCompleteMe'
"LanguageTool
Plugin 'dpelle/vim-LanguageTool'

" Keep Plugin commands between vundle#begin/end.
" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line
